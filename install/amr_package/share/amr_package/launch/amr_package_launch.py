from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package="amr_package",
            executable="OrderOptimizer",
            name="OrderOptimizer",
            output="screen",
            emulate_tty=True,
            parameters=[
                {"path_parameter": "/home/amuntu/knapp_ws/src/amr_package/amr_files/"}
            ]
        )
    ])
