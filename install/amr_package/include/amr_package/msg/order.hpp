// generated from rosidl_generator_cpp/resource/idl.hpp.em
// generated code does not contain a copyright notice

#ifndef AMR_PACKAGE__MSG__ORDER_HPP_
#define AMR_PACKAGE__MSG__ORDER_HPP_

#include "amr_package/msg/detail/order__struct.hpp"
#include "amr_package/msg/detail/order__builder.hpp"
#include "amr_package/msg/detail/order__traits.hpp"

#endif  // AMR_PACKAGE__MSG__ORDER_HPP_
