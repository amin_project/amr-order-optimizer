// generated from rosidl_generator_c/resource/idl__type_support.h.em
// with input from amr_package:msg/Order.idl
// generated code does not contain a copyright notice

#ifndef AMR_PACKAGE__MSG__DETAIL__ORDER__TYPE_SUPPORT_H_
#define AMR_PACKAGE__MSG__DETAIL__ORDER__TYPE_SUPPORT_H_

#include "rosidl_typesupport_interface/macros.h"

#include "amr_package/msg/rosidl_generator_c__visibility_control.h"

#ifdef __cplusplus
extern "C"
{
#endif

#include "rosidl_runtime_c/message_type_support_struct.h"

// Forward declare the get type support functions for this type.
ROSIDL_GENERATOR_C_PUBLIC_amr_package
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(
  rosidl_typesupport_c,
  amr_package,
  msg,
  Order
)();

#ifdef __cplusplus
}
#endif

#endif  // AMR_PACKAGE__MSG__DETAIL__ORDER__TYPE_SUPPORT_H_
