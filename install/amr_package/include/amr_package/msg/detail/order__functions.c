// generated from rosidl_generator_c/resource/idl__functions.c.em
// with input from amr_package:msg/Order.idl
// generated code does not contain a copyright notice
#include "amr_package/msg/detail/order__functions.h"

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>


// Include directives for member types
// Member `description`
#include "rosidl_runtime_c/string_functions.h"

bool
amr_package__msg__Order__init(amr_package__msg__Order * msg)
{
  if (!msg) {
    return false;
  }
  // order_id
  // description
  if (!rosidl_runtime_c__String__init(&msg->description)) {
    amr_package__msg__Order__fini(msg);
    return false;
  }
  return true;
}

void
amr_package__msg__Order__fini(amr_package__msg__Order * msg)
{
  if (!msg) {
    return;
  }
  // order_id
  // description
  rosidl_runtime_c__String__fini(&msg->description);
}

amr_package__msg__Order *
amr_package__msg__Order__create()
{
  amr_package__msg__Order * msg = (amr_package__msg__Order *)malloc(sizeof(amr_package__msg__Order));
  if (!msg) {
    return NULL;
  }
  memset(msg, 0, sizeof(amr_package__msg__Order));
  bool success = amr_package__msg__Order__init(msg);
  if (!success) {
    free(msg);
    return NULL;
  }
  return msg;
}

void
amr_package__msg__Order__destroy(amr_package__msg__Order * msg)
{
  if (msg) {
    amr_package__msg__Order__fini(msg);
  }
  free(msg);
}


bool
amr_package__msg__Order__Sequence__init(amr_package__msg__Order__Sequence * array, size_t size)
{
  if (!array) {
    return false;
  }
  amr_package__msg__Order * data = NULL;
  if (size) {
    data = (amr_package__msg__Order *)calloc(size, sizeof(amr_package__msg__Order));
    if (!data) {
      return false;
    }
    // initialize all array elements
    size_t i;
    for (i = 0; i < size; ++i) {
      bool success = amr_package__msg__Order__init(&data[i]);
      if (!success) {
        break;
      }
    }
    if (i < size) {
      // if initialization failed finalize the already initialized array elements
      for (; i > 0; --i) {
        amr_package__msg__Order__fini(&data[i - 1]);
      }
      free(data);
      return false;
    }
  }
  array->data = data;
  array->size = size;
  array->capacity = size;
  return true;
}

void
amr_package__msg__Order__Sequence__fini(amr_package__msg__Order__Sequence * array)
{
  if (!array) {
    return;
  }
  if (array->data) {
    // ensure that data and capacity values are consistent
    assert(array->capacity > 0);
    // finalize all array elements
    for (size_t i = 0; i < array->capacity; ++i) {
      amr_package__msg__Order__fini(&array->data[i]);
    }
    free(array->data);
    array->data = NULL;
    array->size = 0;
    array->capacity = 0;
  } else {
    // ensure that data, size, and capacity values are consistent
    assert(0 == array->size);
    assert(0 == array->capacity);
  }
}

amr_package__msg__Order__Sequence *
amr_package__msg__Order__Sequence__create(size_t size)
{
  amr_package__msg__Order__Sequence * array = (amr_package__msg__Order__Sequence *)malloc(sizeof(amr_package__msg__Order__Sequence));
  if (!array) {
    return NULL;
  }
  bool success = amr_package__msg__Order__Sequence__init(array, size);
  if (!success) {
    free(array);
    return NULL;
  }
  return array;
}

void
amr_package__msg__Order__Sequence__destroy(amr_package__msg__Order__Sequence * array)
{
  if (array) {
    amr_package__msg__Order__Sequence__fini(array);
  }
  free(array);
}
