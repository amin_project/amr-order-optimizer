// generated from rosidl_typesupport_fastrtps_c/resource/idl__rosidl_typesupport_fastrtps_c.h.em
// with input from amr_package:msg/Order.idl
// generated code does not contain a copyright notice
#ifndef AMR_PACKAGE__MSG__DETAIL__ORDER__ROSIDL_TYPESUPPORT_FASTRTPS_C_H_
#define AMR_PACKAGE__MSG__DETAIL__ORDER__ROSIDL_TYPESUPPORT_FASTRTPS_C_H_


#include <stddef.h>
#include "rosidl_runtime_c/message_type_support_struct.h"
#include "rosidl_typesupport_interface/macros.h"
#include "amr_package/msg/rosidl_typesupport_fastrtps_c__visibility_control.h"

#ifdef __cplusplus
extern "C"
{
#endif

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_amr_package
size_t get_serialized_size_amr_package__msg__Order(
  const void * untyped_ros_message,
  size_t current_alignment);

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_amr_package
size_t max_serialized_size_amr_package__msg__Order(
  bool & full_bounded,
  size_t current_alignment);

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_amr_package
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_fastrtps_c, amr_package, msg, Order)();

#ifdef __cplusplus
}
#endif

#endif  // AMR_PACKAGE__MSG__DETAIL__ORDER__ROSIDL_TYPESUPPORT_FASTRTPS_C_H_
