// generated from rosidl_generator_c/resource/idl.h.em
// with input from amr_package:msg/Order.idl
// generated code does not contain a copyright notice

#ifndef AMR_PACKAGE__MSG__ORDER_H_
#define AMR_PACKAGE__MSG__ORDER_H_

#include "amr_package/msg/detail/order__struct.h"
#include "amr_package/msg/detail/order__functions.h"
#include "amr_package/msg/detail/order__type_support.h"

#endif  // AMR_PACKAGE__MSG__ORDER_H_
