// generated from rosidl_generator_cpp/resource/idl.hpp.em
// generated code does not contain a copyright notice

#ifndef ORDER_INTERFACE__MSG__ORDER_HPP_
#define ORDER_INTERFACE__MSG__ORDER_HPP_

#include "order_interface/msg/detail/order__struct.hpp"
#include "order_interface/msg/detail/order__builder.hpp"
#include "order_interface/msg/detail/order__traits.hpp"

#endif  // ORDER_INTERFACE__MSG__ORDER_HPP_
