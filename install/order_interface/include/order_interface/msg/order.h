// generated from rosidl_generator_c/resource/idl.h.em
// with input from order_interface:msg/Order.idl
// generated code does not contain a copyright notice

#ifndef ORDER_INTERFACE__MSG__ORDER_H_
#define ORDER_INTERFACE__MSG__ORDER_H_

#include "order_interface/msg/detail/order__struct.h"
#include "order_interface/msg/detail/order__functions.h"
#include "order_interface/msg/detail/order__type_support.h"

#endif  // ORDER_INTERFACE__MSG__ORDER_H_
