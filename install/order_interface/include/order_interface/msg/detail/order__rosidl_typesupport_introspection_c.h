// generated from rosidl_typesupport_introspection_c/resource/idl__rosidl_typesupport_introspection_c.h.em
// with input from order_interface:msg/Order.idl
// generated code does not contain a copyright notice

#ifndef ORDER_INTERFACE__MSG__DETAIL__ORDER__ROSIDL_TYPESUPPORT_INTROSPECTION_C_H_
#define ORDER_INTERFACE__MSG__DETAIL__ORDER__ROSIDL_TYPESUPPORT_INTROSPECTION_C_H_

#ifdef __cplusplus
extern "C"
{
#endif


#include "rosidl_runtime_c/message_type_support_struct.h"
#include "rosidl_typesupport_interface/macros.h"
#include "order_interface/msg/rosidl_typesupport_introspection_c__visibility_control.h"

ROSIDL_TYPESUPPORT_INTROSPECTION_C_PUBLIC_order_interface
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, order_interface, msg, Order)();

#ifdef __cplusplus
}
#endif

#endif  // ORDER_INTERFACE__MSG__DETAIL__ORDER__ROSIDL_TYPESUPPORT_INTROSPECTION_C_H_
