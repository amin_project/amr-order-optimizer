// generated from rosidl_generator_cpp/resource/idl__traits.hpp.em
// with input from order_interface:msg/Order.idl
// generated code does not contain a copyright notice

#ifndef ORDER_INTERFACE__MSG__DETAIL__ORDER__TRAITS_HPP_
#define ORDER_INTERFACE__MSG__DETAIL__ORDER__TRAITS_HPP_

#include "order_interface/msg/detail/order__struct.hpp"
#include <rosidl_runtime_cpp/traits.hpp>
#include <stdint.h>
#include <type_traits>

namespace rosidl_generator_traits
{

template<>
inline const char * data_type<order_interface::msg::Order>()
{
  return "order_interface::msg::Order";
}

template<>
inline const char * name<order_interface::msg::Order>()
{
  return "order_interface/msg/Order";
}

template<>
struct has_fixed_size<order_interface::msg::Order>
  : std::integral_constant<bool, false> {};

template<>
struct has_bounded_size<order_interface::msg::Order>
  : std::integral_constant<bool, false> {};

template<>
struct is_message<order_interface::msg::Order>
  : std::true_type {};

}  // namespace rosidl_generator_traits

#endif  // ORDER_INTERFACE__MSG__DETAIL__ORDER__TRAITS_HPP_
