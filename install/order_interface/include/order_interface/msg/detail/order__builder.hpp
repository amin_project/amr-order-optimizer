// generated from rosidl_generator_cpp/resource/idl__builder.hpp.em
// with input from order_interface:msg/Order.idl
// generated code does not contain a copyright notice

#ifndef ORDER_INTERFACE__MSG__DETAIL__ORDER__BUILDER_HPP_
#define ORDER_INTERFACE__MSG__DETAIL__ORDER__BUILDER_HPP_

#include "order_interface/msg/detail/order__struct.hpp"
#include <rosidl_runtime_cpp/message_initialization.hpp>
#include <algorithm>
#include <utility>


namespace order_interface
{

namespace msg
{

namespace builder
{

class Init_Order_description
{
public:
  explicit Init_Order_description(::order_interface::msg::Order & msg)
  : msg_(msg)
  {}
  ::order_interface::msg::Order description(::order_interface::msg::Order::_description_type arg)
  {
    msg_.description = std::move(arg);
    return std::move(msg_);
  }

private:
  ::order_interface::msg::Order msg_;
};

class Init_Order_order_id
{
public:
  Init_Order_order_id()
  : msg_(::rosidl_runtime_cpp::MessageInitialization::SKIP)
  {}
  Init_Order_description order_id(::order_interface::msg::Order::_order_id_type arg)
  {
    msg_.order_id = std::move(arg);
    return Init_Order_description(msg_);
  }

private:
  ::order_interface::msg::Order msg_;
};

}  // namespace builder

}  // namespace msg

template<typename MessageType>
auto build();

template<>
inline
auto build<::order_interface::msg::Order>()
{
  return order_interface::msg::builder::Init_Order_order_id();
}

}  // namespace order_interface

#endif  // ORDER_INTERFACE__MSG__DETAIL__ORDER__BUILDER_HPP_
