// generated from rosidl_generator_c/resource/idl__functions.c.em
// with input from order_interface:msg/Order.idl
// generated code does not contain a copyright notice
#include "order_interface/msg/detail/order__functions.h"

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>


// Include directives for member types
// Member `description`
#include "rosidl_runtime_c/string_functions.h"

bool
order_interface__msg__Order__init(order_interface__msg__Order * msg)
{
  if (!msg) {
    return false;
  }
  // order_id
  // description
  if (!rosidl_runtime_c__String__init(&msg->description)) {
    order_interface__msg__Order__fini(msg);
    return false;
  }
  return true;
}

void
order_interface__msg__Order__fini(order_interface__msg__Order * msg)
{
  if (!msg) {
    return;
  }
  // order_id
  // description
  rosidl_runtime_c__String__fini(&msg->description);
}

order_interface__msg__Order *
order_interface__msg__Order__create()
{
  order_interface__msg__Order * msg = (order_interface__msg__Order *)malloc(sizeof(order_interface__msg__Order));
  if (!msg) {
    return NULL;
  }
  memset(msg, 0, sizeof(order_interface__msg__Order));
  bool success = order_interface__msg__Order__init(msg);
  if (!success) {
    free(msg);
    return NULL;
  }
  return msg;
}

void
order_interface__msg__Order__destroy(order_interface__msg__Order * msg)
{
  if (msg) {
    order_interface__msg__Order__fini(msg);
  }
  free(msg);
}


bool
order_interface__msg__Order__Sequence__init(order_interface__msg__Order__Sequence * array, size_t size)
{
  if (!array) {
    return false;
  }
  order_interface__msg__Order * data = NULL;
  if (size) {
    data = (order_interface__msg__Order *)calloc(size, sizeof(order_interface__msg__Order));
    if (!data) {
      return false;
    }
    // initialize all array elements
    size_t i;
    for (i = 0; i < size; ++i) {
      bool success = order_interface__msg__Order__init(&data[i]);
      if (!success) {
        break;
      }
    }
    if (i < size) {
      // if initialization failed finalize the already initialized array elements
      for (; i > 0; --i) {
        order_interface__msg__Order__fini(&data[i - 1]);
      }
      free(data);
      return false;
    }
  }
  array->data = data;
  array->size = size;
  array->capacity = size;
  return true;
}

void
order_interface__msg__Order__Sequence__fini(order_interface__msg__Order__Sequence * array)
{
  if (!array) {
    return;
  }
  if (array->data) {
    // ensure that data and capacity values are consistent
    assert(array->capacity > 0);
    // finalize all array elements
    for (size_t i = 0; i < array->capacity; ++i) {
      order_interface__msg__Order__fini(&array->data[i]);
    }
    free(array->data);
    array->data = NULL;
    array->size = 0;
    array->capacity = 0;
  } else {
    // ensure that data, size, and capacity values are consistent
    assert(0 == array->size);
    assert(0 == array->capacity);
  }
}

order_interface__msg__Order__Sequence *
order_interface__msg__Order__Sequence__create(size_t size)
{
  order_interface__msg__Order__Sequence * array = (order_interface__msg__Order__Sequence *)malloc(sizeof(order_interface__msg__Order__Sequence));
  if (!array) {
    return NULL;
  }
  bool success = order_interface__msg__Order__Sequence__init(array, size);
  if (!success) {
    free(array);
    return NULL;
  }
  return array;
}

void
order_interface__msg__Order__Sequence__destroy(order_interface__msg__Order__Sequence * array)
{
  if (array) {
    order_interface__msg__Order__Sequence__fini(array);
  }
  free(array);
}
