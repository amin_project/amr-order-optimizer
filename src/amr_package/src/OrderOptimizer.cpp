#include <fstream>
#include <string>
#include <sstream> 
#include <vector>
#include <algorithm> 
#include <thread>
#include <mutex>
#include <cfloat>
#include <memory>
#include <cstdio>
#include <chrono>
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include "geometry_msgs/msg/pose.hpp"
#include "order_interface/msg/order.hpp"
#include "visualization_msgs/msg/marker.hpp"
#include "visualization_msgs/msg/marker_array.hpp"

#define DEBUG 0


using std::placeholders::_1;

struct Point {
    float x;
    float y;
};
struct Part {
    Point p;
    std::string name;
    std::string product_name;
};

bool order_id_found = false;
bool order_path_ready=false;

std::vector<int> products;
std::vector<Part> parts;
std::mutex order_lock;
Point order_dest = {0,0};
Point AMR_p={300,300};

std::string filepath("/home/amuntu/knapp_ws/src/amr_package/amr_files/");



/* -------------- Finding the pruducts for the order -------------- */
void find_products(const std::string& file_path, unsigned int order_id_query){

    std::string line;
    std::string query("order:");
    unsigned int order_id = 0;
    
    int line_num = 1;
    bool id_found = false;

    std::ifstream order_file(file_path);
    if (order_file.is_open())
    {
        while (getline(order_file, line))
        {    	
            if (order_id_found) { //if another thread has already found the order_id

                order_file.close();
                return;
            }

            std::size_t found = line.find(query);
            if (found != std::string::npos) {
                //std::cout << "\"order:\" at position " << found << '\n';

                order_id = stoi(line.substr(found + 6));
                if (order_id_query == order_id) {
                    std::cout << "order: " << order_id << std::endl;
                    id_found = true;
                    break;
                }
            }
            line_num++;
        }

    }

    else {
        std::cout << "Unable to open order file";
        return;
    }

    // finding x and y data
    if (id_found) {

        // shared resource for all threads
        order_lock.lock();
        order_id_found = true;
        order_lock.unlock();

        getline(order_file, line);
        
        std::size_t found = line.find("cx:");
        order_dest.x = std::stof(line.substr(found + 3));
        std::cout << "cx: " << order_dest.x << std::endl;
        getline(order_file, line);
        found = line.find("cy:");
        order_dest.y = std::stof(line.substr(found + 3));
        std::cout << "cy: " << order_dest.y << std::endl;
        // finding product numbers

        std::string temp;
        int product_id;
        std::cout << "products:\n";


        while (getline(order_file, line)) {


            if (line.find("order") != std::string::npos) // all products are found
                break;

            std::stringstream ss;
            ss << line;

            while (!ss.eof()) {


                // reading word by word
                ss >> temp;
                
                // Checking whether the given word is integer  
                if (std::stringstream(temp) >> product_id) {
                    std::cout << product_id << std::endl;
                    // shared resource for all threads
                    order_lock.lock();
                    products.push_back(product_id);
                    order_lock.unlock();
                }
            }
        }
        order_file.close();
    }
  
}



/*            Finding the parts for the products               */

bool find_parts(const std::string& file_path){

  int id;
  std::size_t found;
  std::string line;
  bool product_id_found = false;
  bool success=false;
  std::cout << "Number of products: " << products.size() << std::endl;
  std::sort(products.begin(), products.end());

  std::ifstream products_file(file_path);

  if (products_file.is_open()) {

    std::string temp;
    std::stringstream ss;
    for (int product_id : products) {
        while (getline(products_file, line)) {
                found = line.find("id:");
            if (found != std::string::npos) {
                id = stoi(line.substr(found + 4));
                if (id == product_id) {
                    std::cout << "product: " << product_id << std::endl;
                    product_id_found = true;
                    success=true;
                    break;
                }
            }

        }
        if (product_id_found) {
            product_id_found = false;
            std::string product_name;
            while (getline(products_file, line)) {
                if (line.find("id") != std::string::npos) // all parts are found
                    break;
                else if((found = line.find("product:")) != std::string::npos)
                    product_name = line.substr(found + 9); // save the product name
                else if ((found = line.find("part:")) != std::string::npos) {

                    Part new_part;
                    new_part.product_name = product_name;
                    new_part.name = line.substr(found + 6); // save the part name
                    std::cout << new_part.name << std::endl;
                    getline(products_file, line);
                    if ((found = line.find("cx:")) != std::string::npos) {
                        new_part.p.x = std::stof(line.substr(found + 4));
                        std::cout << new_part.p.x << std::endl;
                    }
                    getline(products_file, line);
                    if ((found = line.find("cy:")) != std::string::npos) {
                        new_part.p.y = std::stof(line.substr(found + 4));
                        std::cout << new_part.p.y << std::endl;
                    }
                    parts.push_back(new_part);
                    continue;
                }


                     found = line.find("id:");
                if (found != std::string::npos) {
                    id = stoi(line.substr(found + 4));
                    if (id == product_id) {
                        std::cout << product_id << std::endl;
                        product_id_found = true;
                        break;
                    }
                }
            }
        }
    }
    products_file.close();
    return success;

  }
  else{
  	std::cout << "Unable to open configuration file";
  	return false;
  }
}



/*     determine the shortest path     */
std::vector<int> shortest_path(std::vector<std::vector<float>>& dist)
{
    int index = 1;
    int j = 0;
    float min_value = FLT_MAX;
    int min_i, min_j;
    int parts_size = dist.size();
    std::vector<int> order(parts_size, 0);
    while (index < parts_size) {
        min_value = FLT_MAX;
        for (int i = 0; i < parts_size; i++) {

            if (dist[i][j] < min_value && i != j && dist[i][j] != -1) {
                min_value = dist[i][j];
                min_i = i;
                min_j = j;

            }
        }
        order[index] = min_i;
        for (int k = 0; k < parts_size; k++){ // invalidate the processed matrix elements
            dist[j][k] = -1;
        }
        dist[min_j][min_i] = -1;
        dist[min_i][min_j] = -1;

        j = min_i; // in the next iteration find the minimum distance in the column j=min_i

        index++;

    }
    
#if DEBUG    
    // print the shortest path 
    std::cout << "shortest path: \n";
    for (int i = 0; i < parts_size; i++)
        std::cout << order[i] << std::endl;
#endif    
    return order;
}


/* calculation of the pairwise distances between the points */
void distance(std::vector<std::vector<float>>& dist)
{    
    std::vector<Point> points;
    points.push_back(AMR_p);
     for (Part part : parts)
        points.push_back(part.p);
     
    for (unsigned int i = 0; i < points.size(); i++)
        for (unsigned int j = 0; j < i; j++) {
            dist[i][j] = sqrt(pow(points[i].x - points[j].x, 2) + pow(points[i].y - points[j].y, 2));
            dist[j][i] = dist[i][j];
        }

#if DEBUG
    std::cout << "\n\n distance matrix\n";
    for (unsigned int i = 0; i < points.size(); i++) {
        for (unsigned int j = 0; j < points.size(); j++)
            std::cout << dist[i][j] << "\t";
        std::cout << "\n";        
    }
#endif    
   
}

/*           processing the order and printing the final result      */ 
void order_processing(unsigned int order_id_query, std::string description)
{

    //std::string path_name("/home/amuntu/knapp_ws/src/amr_package/amr_files/");
    std::string order_folder="orders/";
    std::string order_file_names[5] = { "orders_20201201.yaml", "orders_20201202.yaml","orders_20201203.yaml",
                              "orders_20201204.yaml", "orders_20201205.yaml"};
                              
    // using threads for parsing the order files                          
    std::thread file_thread[5];
    for(int i=0;i<5;i++)
        file_thread[i]=std::thread(find_products, filepath + order_folder + order_file_names[i], order_id_query);        
        

    for (int i = 0; i < 5; i++)
        file_thread[i].join();

    if (order_id_found)
    {
	 //std::string path_name("/home/amuntu/knapp_ws/src/amr_package/amr_files/");
	 std::string configuration_folder="configuration/";
	 std::string configuration_file="products.yaml";
	 bool parts_found=false;
        parts_found=find_parts(filepath + configuration_folder+ configuration_file);
	
	if(parts_found)
	{
		/* calculation of the pairwise distances between the points */
		int parts_size = parts.size(); 
		std::vector<std::vector<float>> dist(parts_size+1, std::vector<float>(parts_size+1, 0)); // including the position of the AMR
		
		distance(dist);
		std::vector<int> order = shortest_path(dist);                

		/*print the path with the part and product info*/
		std::cout << "Working on order "<< order_id_query<<"  ("<<description<<")\n";
		 for (int i = 1; i < (parts_size+1); i++) {
		    std::cout << "Fetching part " << parts[order[i]-1].name << " for product " << parts[order[i]-1].product_name
		        << " at x:"<<parts[order[i]-1].p.x<<", y: "<<parts[order[i]-1].p.y<<"\n";
		}
		std::cout << "Delivering to destination x: " << order_dest.x << ", y:"<< order_dest.y<<"\n";
		
		order_path_ready=true;
	}
      }
      else
      	   std::cout << "order id was not found \n";	
}


// create a marker array for visualization
void markers_setting(visualization_msgs::msg::MarkerArray& pickup_marker)
{

	visualization_msgs::msg::Marker marker;

	marker.header.frame_id = "/my_frame";
	//marker.header.stamp = node->now();

	marker.ns = "pickups";
	marker.id = 0;

	marker.type = 1;
	marker.action = visualization_msgs::msg::Marker::ADD;

	// cube marker for AMR position
	marker.type = 1;
	marker.pose.position.x = AMR_p.x;
	marker.pose.position.y = AMR_p.y;
	marker.pose.position.z = 0;
	marker.pose.orientation.x = 0.0;
	marker.pose.orientation.y = 0.0;
	marker.pose.orientation.z = 0.0;
	marker.pose.orientation.w = 1.0;
	marker.scale.x = 10;
	marker.scale.y = 10;
	marker.scale.z = 10;
	marker.color.a = 1.0;
	marker.color.r = 0.0;
	marker.color.g = 1.0;
	marker.color.b = 0.0;

	//marker.lifetime = rclcpp::Duration();
		
	pickup_marker.markers.push_back(marker);
	
	// blue cylinder markers for part locations
	for(unsigned int i=0;i<parts.size();i++){	

		marker.id = i+1;
		marker.pose.position.x =parts[i].p.x;
		marker.pose.position.y =parts[i].p.y;

		marker.type = 3;
		marker.color.r = 0.0;
		marker.color.g = 0.0;
		marker.color.b = 1.0;
		marker.scale.z = 15;
		pickup_marker.markers.push_back(marker);	
			
	}
	
	// a red sphere marker for destination location
	marker.id = parts.size()+1;
	marker.pose.position.x =order_dest.x;
	marker.pose.position.y =order_dest.y;
	marker.type = 2;
	marker.color.r = 1.0;
	marker.color.g = 0.0;
	marker.color.b = 0.0;
	pickup_marker.markers.push_back(marker);
}


 void topic_callback_pose(const geometry_msgs::msg::Pose::SharedPtr msg) 
    {
    	AMR_p.x=msg->position.x;
    	AMR_p.y=msg->position.y;
    	std::cout<<"I heard current position: x= "<<AMR_p.x<<" , y= "<<AMR_p.y<<"\n";
       //RCLCPP_INFO(this->get_logger(), "I heard the new position");
    }
    
    
  void topic_callback_order(const order_interface::msg::Order::SharedPtr msg) 
    {

    	order_id_found = false;
        products.clear();
        parts.clear();        
        std::cout<<"I heard next order:\n";
        order_processing(msg->order_id,msg->description);
    }   
 
 
    

int main(int argc, char * argv[])
{
 
  rclcpp::init(argc, argv);
  /*if(argc>1)
  {
	printf("Path to Directory: %s\n", argv[1]);
	
  }*/
    

  	auto node = rclcpp::Node::make_shared("OrderOptimizer");
  	
  	//auto& node_ref=node;
  	
  	node->declare_parameter<std::string>("path_parameter", filepath);
  	
  	rclcpp::Parameter parameter=node->get_parameter("path_parameter");
        std::cout<<parameter.value_to_string ()<<"\n";
  	
  	auto pose_subscription=node->create_subscription<geometry_msgs::msg::Pose>(
      "currentPosition", 10, &topic_callback_pose);
      
      // order subscription
      
      auto order_subscription=node->create_subscription<order_interface::msg::Order>(
      "nextOrder", 10, &topic_callback_order);    
      
      
       
       // visualization publish
       auto vis_pub = node->create_publisher<visualization_msgs::msg::MarkerArray>( "order_path", 1);


	       
  	// Running at 10Hz
	rclcpp::Rate loop_rate(10);
	rclcpp::spin_some(node);
	
	std::string temp_param;
		
	visualization_msgs::msg::MarkerArray pickup_marker;

	while (rclcpp::ok()) {	
	
		parameter=node->get_parameter("path_parameter");
        	temp_param=parameter.value_to_string();
        	if(temp_param.compare(filepath)!=0)
        		filepath=temp_param;
		
		if(order_path_ready){
		
			pickup_marker.markers.clear();			
			markers_setting(pickup_marker);
			vis_pub->publish(pickup_marker);
		}	
		rclcpp::spin_some(node);

		loop_rate.sleep();	
	}

  rclcpp::shutdown();
  return 0;
}
